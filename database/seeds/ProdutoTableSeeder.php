<?php

use App\CategoriaProduto;
use App\Produto;
use App\ProdutoAtributo;
use Illuminate\Database\Seeder;

class ProdutoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $burgers = CategoriaProduto::create(['nome' => 'Hamburgers', 'visivel' => true]);
        $porcoes = CategoriaProduto::create(['nome' => 'Porções', 'visivel' => true]);
        $bebidas = CategoriaProduto::create(['nome' => 'Bebidas', 'visivel' => true]);

        $produtos = [
            [
                'categoria_produto_id' => $burgers->id,
                'nome' => 'Burger Tradicional',
                'valor' => 15,
                'visivel' => true,
                'atributos' => [
                    [
                        'atributo' => 'Peso bife',
                        'descricao' => '180 g',
                    ],
                    [
                        'atributo' => 'Possui pimenta'
                    ]
                ]
            ],
            [
                'categoria_produto_id' => $burgers->id,
                'nome' => 'Burger Frango',
                'valor' => 16.5,
                'visivel' => true
            ],
            [
                'categoria_produto_id' => $burgers->id,
                'nome' => 'Burger Bacon',
                'valor' => 16.5,
                'visivel' => true
            ],
            [
                'categoria_produto_id' => $porcoes->id,
                'nome' => 'Porção Fritas P',
                'valor' => 5,
                'visivel' => true
            ],
            [
                'categoria_produto_id' => $porcoes->id,
                'nome' => 'Porção Fritas G',
                'valor' => 8,
                'visivel' => true
            ],
            [
                'categoria_produto_id' => $bebidas->id,
                'nome' => 'Coca-cola 600 mL',
                'valor' => 8,
                'visivel' => true
            ],
            [
                'categoria_produto_id' => $bebidas->id,
                'nome' => 'Coca-cola Lata',
                'valor' => 5,
                'visivel' => true
            ],
        ];

        foreach ($produtos as $produto) {
            $p = Produto::create([
                'categoria_produto_id' => $produto['categoria_produto_id'],
                'nome' => $produto['nome'],
                'descricao' => $produto['descricao'] ?? null,
                'valor' => $produto['valor'],
                'visivel' => $produto['visivel'],
            ]);

            if (isset($produto['atributos'])) {
                foreach ($produto['atributos'] as $atributo) {
                    ProdutoAtributo::create([
                        'atributo' => $atributo['atributo'],
                        'descricao' => $atributo['descricao'] ?? null,
                        'produto_id' => $p->id
                    ]);
                }
            }
        }
    }
}

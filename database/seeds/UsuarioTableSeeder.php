<?php

use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Database\Seeder;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'email' => 'admin@litedelivery.com',
            'password' => Hash::make('teste123'),
            'tipo' => 'ADMIN',
            'habilitado' => true
        ]);

        User::create([
            'name' => 'Atendente',
            'email' => 'atendente@litedelivery.com',
            'password' => Hash::make('teste123'),
            'tipo' => 'ATENDENTE',
            'habilitado' => true
        ]);
    }
}

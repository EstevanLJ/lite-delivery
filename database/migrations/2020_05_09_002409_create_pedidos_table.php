<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('users');
            
            $table->string('situacao');
            $table->decimal('quantidade_itens', 10, 2);
            $table->decimal('valor_total_itens', 10, 2);
            $table->unsignedBigInteger('forma_pagamento_id')->nullable();
            $table->foreign('forma_pagamento_id')->references('id')->on('forma_pagamentos');

            $table->boolean('entregar');
            $table->unsignedBigInteger('endereco_entrega_id')->nullable();
            $table->foreign('endereco_entrega_id')->references('id')->on('enderecos');
            $table->decimal('valor_frete', 10, 2);

            $table->text('observacoes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}

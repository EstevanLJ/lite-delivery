import React from "react";
import { Button, DatePicker, version } from "antd";
import { Switch, Route } from "react-router-dom";

import Page from '../Page';

import UsuariosPage from '../pages/cadastros/Usuarios';
import ProdutosList from '../pages/cadastros/produtos/ProdutosList';
import ProdutosForm from '../pages/cadastros/produtos/ProdutosForm';
import LoginPage from '../pages/auth/Login';

export default () => {

  return (
    <Switch>

      <Route path="/login"><LoginPage /></Route>

      <Route path="/cadastros/usuarios"><UsuariosPage /></Route>

      <Route path="/cadastros/produtos/create" children={<ProdutosForm />} />
      <Route path="/cadastros/produtos/:id" children={<ProdutosForm />} />
      <Route path="/cadastros/produtos"><ProdutosList /></Route>

      <Route path="/">
        <Page>
          <h1>antd version: {version}</h1>
          <DatePicker />
          <Button type="primary" style={{ marginLeft: 8 }}>Primary Button</Button>
        </Page>
      </Route>

    </Switch>
  );
}

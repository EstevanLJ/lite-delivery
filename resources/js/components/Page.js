import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import {
  Link
} from "react-router-dom";
import AppLayout from './AppLayout';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const Page = (props) => {

  const renderBreadcrumbs = () => {
    if (!props.breadcrumbs) {
      return <div style={{ margin: '12px 0' }}></div>;
    }

    return (
      <Breadcrumb style={{ margin: '16px 0' }}>
        {props.breadcrumbs.map((b, i) => (
          <Breadcrumb.Item key={i}>{b}</Breadcrumb.Item>
        ))}
      </Breadcrumb>
    );
  }

  return (
    <>
      <AppLayout>
        {renderBreadcrumbs()}
        <Content
          className="site-layout-background"
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
          }}
        >
          {props.children}
        </Content>
      </AppLayout>
    </>
  );
}

export default Page

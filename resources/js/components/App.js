import React from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
} from "react-router-dom";

import './App.css';
import "antd/dist/antd.css";

import AppRoutes from './config/Routes';


function App() {

  return (
    <div className="App">
      <Router>
        <AppRoutes />
      </Router>
    </div>
  );
}

if (document.getElementById('app-root')) {
  ReactDOM.render(<App />, document.getElementById('app-root'));
}

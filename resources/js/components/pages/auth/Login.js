import React from 'react';
import { Form, Input, Button, Checkbox, Layout } from 'antd';
import axios from 'axios';

const { Header, Content, Sider } = Layout;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export default (props) => {
  const onFinish = values => {
    console.log('Success:', values);
    
    axios.post('/login', {
      email: values.username,
      password: values.password,
    }).then(response => {
      window.location.replace("/home");
    });
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };


  React.useEffect(() => {
    //Inicializa Cookie CSRF
    axios.get('/sanctum/csrf-cookie').then(response => {
    });
  }, [])

  return (
    <Content
      className="site-layout-background"
      style={{
        padding: 24,
        margin: 0,
        minHeight: 280,
      }}
    >
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout} name="remember" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
            </Button>
        </Form.Item>
      </Form>
    </Content>

  );
}
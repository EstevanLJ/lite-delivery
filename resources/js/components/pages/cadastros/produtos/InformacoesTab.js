import React from 'react';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Checkbox, Select, Spin, message } from 'antd';

const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

const tailLayout = {
  wrapperCol: { offset: 4, span: 20 },
};

export default (props) => {

  const [form] = Form.useForm();
  const [id, setId] = React.useState(null);
  const [categorias, setCategorias] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const history = useHistory();

  React.useEffect(() => {
    axios.get('/api/categorias-produtos').then((res) => {
      setCategorias(res.data.data);
    });
  }, []);

  React.useEffect(() => {
    if (props.data) {
      setId(props.data.id);
      form.setFieldsValue({
        nome: props.data.nome,
        categoria_produto_id: props.data.categoria_produto_id,
        descricao: props.data.descricao,
        valor: props.data.valor,
        visivel: props.data.visivel,
      });
    }
  }, [props.data]);

  const onFinish = values => {
    setLoading(true);

    axios({
      url: id ? `/api/produtos/${id}` : '/api/produtos',
      method: id ? 'PUT' : 'POST',
      data: values
    }).then((res) => {
      message.success(id ? 'Atualizado com sucesso' : 'Cadastrado com sucesso');

      if (!id) {
        history.push(`/cadastros/produtos/${res.data.data.id}`);
      }

    }).catch((res) => {
      message.error('Ocorreu um erro. Tente novamente');
    }).finally(() => setLoading(false));
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };


  return (
    <Spin spinning={loading}>
      <Form
        {...layout}
        name="produto-informacoes"
        form={form}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >

        <Form.Item name="categoria_produto_id" label="Categoria" rules={[{ required: true }]}>
          <Select placeholder="Selecione a categoria">
            {categorias.map((c) => (
              <Option value={c.id} key={c.id}>{c.nome}</Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label="Nome"
          name="nome"
          rules={[{ required: true, message: 'Por favor, insira o nome do produto' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Descrição"
          name="descricao"
          rules={[{ required: false }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Valor"
          name="valor"
          rules={[{ required: true, message: 'Por favor, insira o valor do produto' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout} name="visivel" valuePropName="checked">
          <Checkbox>Visível?</Checkbox>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Salvar
        </Button>
        </Form.Item>

      </Form>
    </Spin>
  );
}

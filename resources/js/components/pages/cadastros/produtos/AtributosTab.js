import React from 'react';
import { PlusOutlined } from '@ant-design/icons';
import axios from 'axios';
import { Table, Button, Modal, Form, Input } from 'antd';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Atributo',
    dataIndex: 'atributo',
    key: 'atributo',
  },
  {
    title: 'Descrição',
    dataIndex: 'descricao',
    key: 'descricao',
  },
  {
    title: 'Ações',
    key: 'acoes',
    width: 250,
    align: 'center',
    render: a => (
      <>
        <Button type="primary" className="mr-2">Editar</Button>
        <Button danger>Remover</Button>
      </>
    ),
  },
];

export default (props) => {

  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState([]);
  const [modal, setModal] = React.useState(false);
  const [form] = Form.useForm();

  React.useEffect(() => {
    if (props.data) {
      const processedData = props.data.atributos.map((d) => (
        {
          ...d,
          key: d.id
        }
      ));
      setData(processedData);
    }
  }, [props.data]);

  const novoRegistro = () => {
    setModal(true)
  }

  const onSave = () => {
    setLoading(true);
    setTimeout(() => {
      setModal(false)
      setLoading(false);
    }, 3000);
  }

  return (
    <>
      <Table dataSource={data} columns={columns} />

      <Button type="primary" icon={<PlusOutlined />} onClick={novoRegistro}>
        Adicionar
      </Button>

      <Modal
        title="Atributo"
        visible={modal}
        onOk={onSave}
        onCancel={() => setModal(false)}
        footer={[
          <Button key="back" onClick={() => setModal(false)}>
            Return
          </Button>,
          <Button key="submit" type="primary" loading={loading} onClick={onSave}>
            Submit
          </Button>,
        ]}
      >
        <Form
          name="produto-atributo"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          form={form}
        // onFinish={onFinish}
        // onFinishFailed={onFinishFailed}
        >

          <Form.Item
            label="Atributo"
            name="atributo"
            rules={[{ required: false }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Descrição"
            name="descricao"
            rules={[{ required: false }]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Salvar
            </Button>
          </Form.Item>

        </Form>
      </Modal>
    </>
  );
}



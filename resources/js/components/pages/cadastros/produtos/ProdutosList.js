import React from 'react';
import Page from '../../../Page';
import { PlusOutlined } from '@ant-design/icons';
import { Table, Button, Popconfirm, Typography, message } from 'antd';
import { useHistory } from "react-router-dom";
import axios from 'axios';

const { Title } = Typography;

const getColumns = (history, onDelete) => {
  return [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      sorter: true,
    },
    {
      title: 'Nome',
      dataIndex: 'nome',
      key: 'nome',
      sorter: true,
    },
    {
      title: 'Categoria',
      key: 'categoria_produto_id',
      sorter: true,
      render: categoria => (
        <>
          {categoria.categoria.nome}
        </>
      ),
    },
    {
      title: 'Ações',
      key: 'acoes',
      width: 250,
      align: 'center',
      render: a => (
        <>
          <Button type="primary" className="mr-2" onClick={() => history.push(`/cadastros/produtos/${a.id}`)}>Editar</Button>

          <Popconfirm
            title="Tem certeza que deseja deletar?"
            onConfirm={() => onDelete(a)}
            okText="Sim"
            cancelText="Não"
          >
            <Button danger>Remover</Button>
          </Popconfirm>
        </>
      ),
    },
  ];
}

export default () => {

  const [loading, setLoading] = React.useState(true);
  const [sorting, setSorting] = React.useState(null);
  const [data, setData] = React.useState([]);
  const history = useHistory();

  const onDelete = (item) => {
    setLoading(true);
    axios.delete(`/api/produtos/${item.id}`).then((res) => {
      message.success('Removido com sucesso!');
      getData();
    }).catch((res) => {
      message.error('Ocorreu um erro. Tente novamente');
    }).finally(() => setLoading(false));
  }

  const getData = () => {

    let url = '/api/produtos';

    if (sorting) {
      url += `?column=${sorting.column}&order=${sorting.order}`;
    }

    axios.get(url).then((res) => {
      const processedData = res.data.data.map((d) => (
        {
          ...d,
          key: d.id
        }
      ));
      setData(processedData);
    }).finally(() => setLoading(false));
  }

  const handleTableChange = (pagination, filters, sorter, extra) => {
    if (sorter.order) {
      setSorting({
        column: sorter.columnKey,
        order: sorter.order === 'ascend' ? 'asc' : 'desc'
      });
    } else {
      setSorting(null);
    }
    getData();
  }

  const columns = getColumns(history, onDelete);

  React.useEffect(() => {
    getData();
  }, []);

  return (
    <Page breadcrumbs={['Cadastros', 'Produtos']}>

      <Button type="primary" className="float-right" icon={<PlusOutlined />} onClick={() => history.push('/cadastros/produtos/create')}>
        Adicionar
      </Button>

      <Title level={2}>Produtos</Title>

      <Table
        dataSource={data}
        columns={columns}
        onChange={handleTableChange}
        loading={loading} />

    </Page>
  );
}



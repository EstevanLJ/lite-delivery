import React from 'react';
import axios from 'axios';

import { Button, Tabs, Typography, Spin } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { useParams, useHistory } from "react-router-dom";

import Page from '../../../Page';
import ImagensTab from './ImagensTab'
import AtributosTab from './AtributosTab'
import InformacoesTab from './InformacoesTab'
const { TabPane } = Tabs;
const { Title } = Typography;


export default () => {

  const [loading, setLoading] = React.useState(true);
  const [data, setData] = React.useState(null);
  let { id } = useParams();
  const history = useHistory();

  React.useEffect(() => {
    if (id) {
      axios.get(`/api/produtos/${id}`).then((res) => {
        setData(res.data.data)
        setLoading(false);
      })
    } else {
      setLoading(false);
    }
  }, []);

  return (
    <Page breadcrumbs={['Cadastros', 'Produtos']}>

      <Button className="float-right" icon={<ArrowLeftOutlined />} onClick={() => history.push('/cadastros/produtos')}>
        Voltar
      </Button>

      <Title level={2}>{ data ? data.nome : 'Cadastrar' }</Title>

      <Spin spinning={loading}>

        <Tabs defaultActiveKey="1">
          <TabPane tab="Informações" key="1">
            <InformacoesTab data={data} />
          </TabPane>
          <TabPane tab="Atributos" key="2">
            <AtributosTab data={data} />
          </TabPane>
          <TabPane tab="Imagens" key="3">
            <ImagensTab data={data} />
          </TabPane>
        </Tabs>

      </Spin>

    </Page>
  );
}



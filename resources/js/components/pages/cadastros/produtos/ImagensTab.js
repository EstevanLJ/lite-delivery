import React from 'react';
import axios from 'axios';

import { Table, Button } from 'antd';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Descrição',
        dataIndex: 'descricao',
        key: 'descricao',
    },
    {
        title: 'Ordem',
        dataIndex: 'ordem',
        key: 'ordem',
    },
    {
        title: 'Ações',
        key: 'acoes',
        width: 250,
        align: 'center',
        render: a => (
            <>
                <Tooltip title="Editar">
                    <Button type="primary" className="mr-2">Editar</Button>
                </Tooltip>

                <Tooltip title="Remover">
                    <Button danger>Remover</Button>
                </Tooltip>
            </>
        ),
    },
];

export default (props) => {
    const [data, setData] = React.useState([]);
    return (
        <Table dataSource={data} columns={columns} />
    );
}



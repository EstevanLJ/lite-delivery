import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import {
  Link
} from "react-router-dom";
import axios from 'axios';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const AppLayout = (props) => {

  const logout = () => {
    axios.post('/logout', (res) => {
      window.location.replace("/login");
    });
  }

  return (
    <Layout>
      <Header className="header">
        <div className="site-logo">Lite Delivery</div>
        <Menu theme="dark" mode="horizontal" style={{float: 'right'}}>
          <Menu.Item key="1" onClick={() => logout()}>Sair</Menu.Item>
          {/* <Menu.Item key="2">nav 2</Menu.Item> */}
          {/* <Menu.Item key="3">nav 3</Menu.Item> */}
        </Menu>
      </Header>
      <Layout>
        <Sider width={200} className="site-layout-background">
          <Menu
            mode="inline"
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            style={{ height: '100%', borderRight: 0 }}
          >
            <SubMenu key="sub1" icon={<UserOutlined />} title="Início">
              <Menu.Item key="1">
                <Link to="/home">Home</Link>
              </Menu.Item>
            </SubMenu>
            <SubMenu key="sub2" icon={<LaptopOutlined />} title="Cadastros">
              <Menu.Item key="1">
                <Link to="/cadastros/usuarios">Users</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/cadastros/produtos">Produtos</Link>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout style={{ padding: '0 24px 24px', minHeight: 'calc(100vh - 64px)' }}>
          {props.children}
        </Layout>
      </Layout>
    </Layout>
  );
}

export default AppLayout

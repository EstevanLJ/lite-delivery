<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $table = 'enderecos';

    protected $fillable = [
        'usuario_id', 'tipo', 'favorito',
        'endereco', 'numero', 'complemento', 'ponto_referencia', 
        'cidade_id', 'bairro_id'
    ];

    public function usuario() 
    {
        return $this->hasOne('App\User', 'id', 'usuario_id');
    }

    public function cidade() 
    {
        return $this->hasOne('App\Cidade', 'id', 'cidade_id');
    }

    public function bairro() 
    {
        return $this->hasOne('App\Bairro', 'id', 'bairro_id');
    }
}

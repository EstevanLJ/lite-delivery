<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoProduto extends Model
{
    protected $table = 'pedido_produtos';

    protected $fillable = [
        'pedido_id', 'produto_id',
        'quantidade', 'valor_unitario', 'observacoes', 
    ];

    public function pedido() 
    {
        return $this->belongsTo('App\Pedido', 'id', 'pedido_id');
    }

    public function produto() 
    {
        return $this->belongsTo('App\Produto', 'id', 'produto_id');
    }
}

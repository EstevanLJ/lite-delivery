<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = 'pedidos';

    protected $fillable = [
        'usuario_id', 'situacao', 'observacoes',
        'quantidade_itens', 'valor_total_itens', 'forma_pagamento_id', 
        'entregar', 'endereco_entrega_id',  'valor_frete', 
    ];

    public function usuario() 
    {
        return $this->hasOne('App\User', 'id', 'usuario_id');
    }

    public function enderecoEntrega() 
    {
        return $this->hasOne('App\Endereco', 'id', 'endereco_entrega_id');
    }

    public function bairro() 
    {
        return $this->hasOne('App\Bairro', 'id', 'bairro_id');
    }

    public function formaPagamento()
    {
        return $this->hasOne('App\FormaPagamento', 'id', 'forma_pagamento_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produto extends Model
{
    use SoftDeletes;
    
    protected $table = 'produtos';

    protected $fillable = [
        'nome', 'descricao', 'categoria_produto_id', 'valor', 'visivel'
    ];

    public function categoria()
    {
        return $this->belongsTo('App\CategoriaProduto', 'categoria_produto_id', 'id');
    }
    
    public function imagens()
    {
        return $this->hasMany('App\ProdutoImagem', 'id', 'produto_id');
    }

    public function atributos()
    {
        return $this->hasMany('App\ProdutoAtributo', 'produto_id', 'id');
    }
}

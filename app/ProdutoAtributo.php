<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoAtributo extends Model
{
    protected $table = 'produto_atributos';

    protected $fillable = [
        'produto_id', 'atributo', 'descricao'
    ];

    public function produto()
    {
        return $this->belongsTo('App\Produto', 'id', 'produto_id');
    }
}

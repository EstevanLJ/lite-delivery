<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoImagem extends Model
{
    protected $table = 'produto_imagens';

    protected $fillable = [
        'produto_id', 'descricao', 'ordem', 'arquivo'
    ];

    public function produto()
    {
        return $this->belongsTo('App\Produto', 'id', 'produto_id');
    }
}

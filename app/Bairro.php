<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bairro extends Model
{
    protected $table = 'bairro';

    protected $fillable = [
        'cidade_id', 'nome', 'valor_frete'
    ];

    public function cidade() 
    {
        return $this->hasOne('App\Cidade', 'id', 'cidade_id');
    }
}

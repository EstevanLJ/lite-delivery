<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = 'cidades';

    protected $fillable = [
        'nome', 'uf'
    ];

    public function bairros() 
    {
        return $this->hasMany('App\Bairro', 'id', 'cidade_id');
    }
}

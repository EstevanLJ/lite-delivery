<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaProduto extends Model
{
    protected $table = 'categoria_produtos';

    protected $fillable = [
        'nome', 'visivel'
    ];

    public function produtos() 
    {
        return $this->hasMany('App\Produto', 'id', 'categoria_produto_id');
    }
}

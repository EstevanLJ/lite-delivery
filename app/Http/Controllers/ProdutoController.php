<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProdutoRequest;
use App\ProdutoAtributo;
use App\ProdutoImagem;
use App\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $produtos = Produto::with('categoria', 'atributos');
        
        if ($request->has('order')) {
            $produtos->orderBy($request->input('column'), $request->input('order'));
        }
        
        return [
            'data' => $produtos->get()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProdutoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoRequest $request)
    {
        $validated = $request->validated();
        $produto = Produto::create($validated);

        return [
            'data' => $produto
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $produto)
    {
        $produto->load('atributos', 'imagens');
        
        return [
            'data' => $produto
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProdutoRequest  $request
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoRequest $request, Produto $produto)
    {
        $validated = $request->validated();
        $produto->update($validated);

        return [
            'data' => $produto
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produto $produto)
    {
        $produto->delete();
    }
}

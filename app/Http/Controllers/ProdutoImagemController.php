<?php

namespace App\Http\Controllers;

use App\ProdutoImagem;
use Illuminate\Http\Request;

class ProdutoImagemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdutoImagem  $produtoImagem
     * @return \Illuminate\Http\Response
     */
    public function show(ProdutoImagem $produtoImagem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdutoImagem  $produtoImagem
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdutoImagem $produtoImagem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdutoImagem  $produtoImagem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdutoImagem $produtoImagem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdutoImagem  $produtoImagem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdutoImagem $produtoImagem)
    {
        //
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'nome' => 'required|string',
                    'descricao' => 'nullable|string',
                    'categoria_produto_id' => 'required|integer|exists:categoria_produtos,id',
                    'valor' => 'required|numeric',
                    'visivel' => 'required|boolean'
                ];
            case 'PUT':
                return [
                    'nome' => 'required|string',
                    'descricao' => 'nullable|string',
                    'categoria_produto_id' => 'required|integer|exists:categoria_produtos,id',
                    'valor' => 'required|numeric',
                    'visivel' => 'required|boolean'
                ];
        }
    }
}
